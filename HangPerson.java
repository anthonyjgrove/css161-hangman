
/**
 *        This is a program that runs the hang man game. First takes a word from a list of words in a text file.  
 *        Turns that word from a string to an array of charecters. After that prints out how to play.  
 *        Takes an array initializes all values to the array to be blank spots int he game which are marked with "_ " 
 *        It then asks you what letter you would like to guess.  It then takes the letter it guessed, 
 *        Switches the letter from the blank char " _" to the letter of the letter that was guessed if it is in the word.
 *        If the letter guessed was not guessed it adds one to the wrong counter. Once the wrong counter hits 7 the player loses. 
 *        prints out the gallows and the body depending on the number of wrong guesses that have occured then prints out all the letters that have been guessed so far.
 *        Then  prints out all the letters that have been guessed so far.  If the person guesses all the right letters the person wins.
 *        
 *         
 * @author Anthony Grove
 * @version css 161 - Assign8 
 */
import java.util.*;
public class HangPerson
{
    int gameNumber = 0;
    int max = 100;
    char[] word ;
    char[] lettersGuessed = new char[50];
    String[] holder = new String[100];
    String[] wordArray = new String[100];
    char[] displayedletters;
    int guesses = 0;
    int wrong = 0;
    char guess = ' ';
    String strWord;
    public HangPerson(Scanner input) {
        int count = 0;
        //reads all the words from the file and stores them in a holder array,
        //also counts how many words are in the file
        while(input.hasNextLine()){

            holder[count] = input.nextLine();
            count++;

        }
        //sets wordArray length to the amount in the file.
        wordArray = new String[count+1];
        //transfer words from holder to wordArray witht he length set to the amount of words
        for(int i = 0; i < (count+1); i++){

            wordArray[i] =  holder[i];

        }
        
    }   

    public void play() {
        //starts back over at the top of the word list
        if (gameNumber >= wordArray.length-1 ) {

            gameNumber = 0;
        }
        //picks the word out of the array based on the number of games played
        strWord = wordArray[gameNumber];
        // turns the string into a char array
        word = strWord.toCharArray();
        // there all next ones counter values to zero
        int win = 0;
        guesses = 0;
        wrong = 0;
        // clears arrays
        lettersGuessed = new char[26];
        displayedletters = new char[word.length];
        initializedisplayedletters(displayedletters);
        // displays game 
        displayGallows(wrong);
        printLettersGuessed(lettersGuessed);
        PrintWrongGuesses(wrong);
        PrintWordSoFar(displayedletters);
        //while the person has less than 8 wrong guesses they get to keep playing
        while (wrong < 8) {
            System.out.print("Choose a letter => ");
            Scanner keyboard = new Scanner(System.in);
            char guess = keyboard.next().charAt(0);
            //checks to see if letter has been guessed, if not it contunies 
            if(!getValidLetter(guess, lettersGuessed)){
                //passes the letter guessed into array of letters guessed
                lettersGuessed[guesses] = guess;
                guesses++;
                int check = 0;
                //loops through the word to see if the letter guessed is in there
                for(int i = 0; i < word.length; i++){
                    if (guess == word[i]){

                        displayedletters[i] = word[i];
                        win++;//win counter, 
                        check++;
                    }

                }
                //check int will be 0 if the guessed letter was not in the word
                if(check == 0){
                    wrong++;
                    System.out.println();
                    System.out.println("Sorry, that letter is not in the word!");

                }
                displayGallows(wrong);
                printLettersGuessed(lettersGuessed);
                PrintWrongGuesses(wrong);
                PrintWordSoFar(displayedletters);
                //ends game if you win, does this by matching win counter with the length of the word
                if(win >= word.length){
                    System.out.println("You win!");
                    gameNumber++;
                    break;

                }
                System.out.println();
                //ends game if you lose, does this if wrong counter gets to 7
                if(wrong >= 7){

                    System.out.println("The word was ==> " + strWord); 
                    System.out.println("You lose! :(");
                    gameNumber++;
                    break;
                }

            }
        }
    }
    //this prints out the intro/ explains how to play.
    public void displayGameIntro() {

        System.out.println("Welcome to the hangperson game ...");
        System.out.println("To play, guess a letter to try to guess the word.");
        System.out.println("Every time you choose an incorrect letter another");
        System.out.println("body part appears on the gallows. If you guess the");
        System.out.println("word before you're hung, you win :-)");
        System.out.println("If you get hung you lose :-(");
        System.out.println("    Time to guess ...");
        System.out.println();

    }
    //Takes an array initializes all values to the array to be blank spots int he game which are marked with "_ " .
    private void initializedisplayedletters(char[] a)  {
        for(int i = 0; i < a.length; i++) {
            a[i]= '_';
        }

    }
    //switches the letters from the blank char " _" to the letter of the letter that was guessed if it is in the word.
    public void WordSoFar(char guess, char[] word, char[]displayedletters ){
        for(int i = 0; i < word.length; i++) {

            if (guess == word[i]){

                displayedletters[i] = word[i];

            }

        }
    }
    // this method prints out all the letters that have been guessed so far.
    public void printLettersGuessed(char[] a) {
        System.out.print("Letters guessed already => ");
        for(int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");

        }
        System.out.println();
    }
    // this method prints out the number of wrong guesses that have occured
    public void PrintWrongGuesses(int wrong) {

        System.out.println("Number of wrong guesses => " + wrong);
    }
    // this method prints out all the letters that have been guessed so far.
    public void PrintWordSoFar(char[] a) {
        System.out.print("The word so far => ");
        for(int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");

        }
        System.out.println();
    }
    //this method takes in the letter guessed and the letters guessed array.  If the letter guessed is already in the array then it returns true, otherwise it returns false.
    public boolean getValidLetter(char guess, char[] a) {
        for(int i = 0; i < a.length; i++) {

            if(a[i] == guess) {
                System.out.println();
                System.out.println("That letter has already been guessed!");
                System.out.println();
                return true;
            }

        }
        return false;
    }
    //prints out the gallows and the body depending on the number of wrong guesses that have occured
    public void displayGallows(int wrong) {
        System.out.println("|-----|-");
        System.out.print("|");
        //head
        if(wrong > 0) 
        {      System.out.print("     O"); }
        System.out.println();
        System.out.print("|");
        //left arm
        if(wrong > 3) 
        {      System.out.print("    \\"); }
        else
        {      System.out.print("     "); }
        //upper body        
        if(wrong > 1) 
        {      System.out.print("|"); }
        //right arm
        if(wrong > 4) 
        {      System.out.print("/"); }
        System.out.println();
        System.out.print("|");
        if(wrong > 2) 
        {      System.out.print("     |"); }
        System.out.println();
        System.out.print("|");
        //legs
        if(wrong > 5) 
        {      System.out.print("    /"); }
        if(wrong > 6) 
        {      System.out.print(" \\"); }
        System.out.println();
        System.out.print("|__________");
        System.out.println();
    }
}
